from datetime import datetime
from typing import List, Optional


class Author:
    """Author the book """

    def __init__(self, first_name: str, last_name: str, year_of_birth: Optional[int] = None) -> None:
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __eq__(self, other: "Author") -> bool:
        if not isinstance(other, Author):
            raise TypeError(f"for type Author and type {type(other)} operation is not implemented")
        return (self.first_name == other.first_name and
                self.last_name == other.last_name and
                self.year_of_birth == other.year_of_birth)

    def __repr__(self) -> str:
        return f"Author({self.first_name}, {self.last_name}, {self.year_of_birth})"

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name}"

    def __hash__(self):
        return hash((self.first_name, self.last_name, self.year_of_birth))


class Genre:
    """Genre books"""

    def __init__(self, name: str, description: Optional[str] = None) -> None:
        self.name = name
        self.description = description

    def __repr__(self) -> str:
        return f"Genre({self.name}, {self.description})"

    def __str__(self) -> str:
        return f"{self.name}"


class Book:
    """Book"""

    def __init__(self, name: str, language: str, year: datetime.year, /,
                 *authors: Author,
                 genres: Optional[List[Genre]] = None, isbn: Optional[str] = None, description: Optional[str] = None
                 ) -> None:
        self.name = name
        self.description = description
        self.language = language
        self.authors = authors
        self.genres = genres
        self.year = year
        self.isbn = isbn

    def __eq__(self, other: "Book") -> bool:
        if not isinstance(other, Book):
            raise TypeError(f"for type Book and type {type(other)} operation is not implemented")
        return self.name == other.name and set(self.authors) == set(other.authors)

    @property
    def age(self) -> int:
        return datetime.now().year - self.year

    @age.setter
    def age(self, value: int) -> None:
        self.age = value

    @age.deleter
    def age(self) -> None:
        del self.age

    def __repr__(self) -> str:
        return (f"Book({self.name}, {self.description}, {self.language}, "
                f"{self.authors}, {self.genres}, {self.year}, {self.isbn})")

    def __str__(self) -> str:
        return f"{self.name}, {self.authors}, {self.year}"

    @age.setter
    def age(self, value):
        self._age = value


b1 = Book("book1", "ua", 1989, isbn="lfkgjdflk")
print(b1.isbn)
b1.__class__.book_count = 1
b2 = Book("gfd", "ya", 2023)
b2.__class__.book_count += 1
print(Book.book_count)
