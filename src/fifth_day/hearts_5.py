import random
import sys
from colorama import Fore, Back, Style
from typing import Tuple, List, Sequence, TypeVar, Optional


class Card:
    SUITS = "♠️ ♡ ♢ ♣️".split()
    RANKS = "2 3 4 5 6 7 8 9 10 J Q K A".split()

    def __init__(self, suit: str, rank: str) -> None:
        if suit not in self.SUITS or rank not in self.RANKS:
            raise ValueError(f"value of suit must be from {self.SUITS} and {self.RANKS}, but got {suit}{rank}")
        self.suit = suit
        self.rank = rank

    def __repr__(self) -> str:
        if self.suit in "♡ ♢":
            return f"{Fore.RED}{self.suit}{self.rank}"
        return f"{Fore.BLACK}{self.suit}{self.rank}"

    @property
    def value(self) -> int:
        """The value of a card is rank a number"""
        return self.RANKS.index(self.rank)

    @property
    def points(self) -> int:
        """Points this cart is worth"""
        if self.suit == "♠️" and self.rank == "Q":
            return 13
        if self.suit == "♡":
            return 1
        return 0

    def __eq__(self, other: "Card") -> bool:
        if not isinstance(other, Card):
            raise TypeError(f"unsupported operand type(s) for ==: 'Card' and '{type(other)}'")
        return self.suit == other.suit and self.rank == other.rank

    def __lt__(self, other: "Card") -> bool:
        if not isinstance(other, Card):
            raise TypeError(f"unsupported operand type(s) for ==: 'Card' and '{type(other)}'")
        return self.value < other.value


class Deck:
    def __init__(self, cards: List[Card]) -> None:
        self.cards = cards

    @classmethod
    def create(cls, shuffle=False):
        """Create a new deck of 52 cards"""
        cards = [Card(s, r) for s in Card.SUITS for r in Card.RANKS]
        if shuffle:
            random.shuffle(cards)
        return cls(cards)

    def deal(self, num_hands: int):
        """Deal the cards in the deck into num_hands"""
        cls = self.__class__
        return tuple(cls(self.cards[i::num_hands]) for i in range(num_hands))


class Player:
    def __init__(self, name: str, hand: Deck) -> None:
        self.name = name
        self.hand = hand

    def play_card(self) -> Card:
        """Play a card from the player's hand."""
        card = random.choice(self.hand.cards)
        self.hand.cards.remove(card)
        print(Back.LIGHTWHITE_EX + Style.BRIGHT + Fore.CYAN + f"{self.name}:{card!r:<3}", end="\t\t")
        return card


class Game:
    def __init__(self, *names: str) -> None:
        deck = Deck.create(shuffle=True)
        self.names = (list(names) + "P1 P2 P3 P4".split())[:4]
        self.hands = {
            n: Player(n, h) for n, h in zip(self.names, deck.deal(4))
        }

    def player_order(self, start=None):
        if start is None:
            start = random.choice(self.names)
        start_idx = self.names.index(start)
        return self.names[start_idx:] + self.names[:start_idx]

    def play(self) -> None:
        """Play a card game."""
        start_player = random.choice(self.names)
        turn_order = self.player_order(start_player)

        while self.hands[start_player].hand.cards:
            for name in turn_order:
                self.hands[name].play_card()
            print()


if __name__ == "__main__":
    player_names = sys.argv[1:]
    game = Game(*player_names)
    game.play()
